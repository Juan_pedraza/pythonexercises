# Obtener un diccionario a partir de dos listas

lista_uno = [1,2,3]
lista_dos = ['Aprendiendo', 'un poco', 'de python']

# dict y zip pueden ir en una misma línea pero de momento lo dejaremos así

mi_diccionario = zip(lista_uno,lista_dos)

mi_diccionario = dict(mi_diccionario)

print(mi_diccionario)