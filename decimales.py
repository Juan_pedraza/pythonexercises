# Mostrar solo dos posiciones de un número decimal

decimal = 3.1416
print('{0:.2f}'.format(decimal))

decimal = 5
print('{0:.2f}'.format(decimal))